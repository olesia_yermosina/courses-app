import React from 'react';

import './Header.css';

import Logo from './components/Logo/Logo';
import Button from '../../common/Button/Button';
import { Link, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { fetchLogout } from '../../store/user/thunk';
import { getIsAuth, getUserName } from '../../store/selectors';

const Header = () => {
	const dispatch = useDispatch();

	const navigate = useNavigate();

	const username = useSelector(getUserName);
	const isAuth = useSelector(getIsAuth);

	const clearLocalStorage = () => {
		localStorage.removeItem('token');
		localStorage.removeItem('name');
		localStorage.removeItem('email');
	};

	return (
		<div data-testid='header' className='Header'>
			<Logo data-testid='logo' />
			<div>
				{isAuth && <Link to={'courses'}>Courses</Link>}
				{isAuth && (
					<span id='name' className='name'>
						{username}
					</span>
				)}
				{isAuth && (
					<Button
						controls={{
							buttonText: 'Logout',
							onClick: (e) => {
								e.preventDefault();
								dispatch(fetchLogout());
								clearLocalStorage();
								navigate('login');
							},
						}}
					/>
				)}
			</div>
		</div>
	);
};

export default Header;
