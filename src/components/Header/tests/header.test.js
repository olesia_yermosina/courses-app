import React from 'react';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

import Header from '../Header';

const mockedState = {
	user: {
		isAuth: true,
		name: 'Test Name',
	},
	courses: [],
	authors: [],
};
const mockedStore = {
	getState: () => mockedState,
	subscribe: jest.fn(),
	dispatch: jest.fn(),
};
describe('Header', () => {
	it('should create Header with mocked user', () => {
		render(
			<Provider store={mockedStore}>
				<BrowserRouter>
					<Header />
				</BrowserRouter>
			</Provider>
		);

		expect(screen.getByText('Test Name')).toBeInTheDocument();
	});
	it('should create Header with logo', () => {
		render(
			<Provider store={mockedStore}>
				<BrowserRouter>
					<Header />
				</BrowserRouter>
			</Provider>
		);

		expect(screen.getByAltText('Logo')).toBeInTheDocument();
	});
});

/*import React from 'react';
import { render, unmountComponentAtNode } from 'react-dom';
//import { render } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import '@testing-library/jest-dom';

import Header from '../Header';
import { Provider, useSelector } from 'react-redux';
import { screen, queryByAltText, queryByText } from '@testing-library/dom';

let container = null;

const mockedState = {
	user: {
		isAuth: true,
		name: 'Test Name',
	},
	courses: [],
	authors: [],
};
const mockedStore = {
	getState: () => mockedState,
	subscribe: jest.fn(),
	dispatch: jest.fn(),
};

beforeEach(() => {
	// setup a DOM element as a render target
	container = document.createElement('div');
	document.body.appendChild(container);
});

afterEach(() => {
	// cleanup on exiting
	unmountComponentAtNode(container);
	container.remove();
	container = null;
});

jest.mock('react-redux');

describe('Header', () => {
	it('should create Header with mocked user', () => {
		const component = render(
			<Provider store={mockedStore}>
				<Header />
			</Provider>,
			container
		);

		expect(component).toMatchSnapshot();
	});

	it('should create Header with Logo', () => {
		render(
			<Provider store={mockedStore}>
				<Header />
			</Provider>,
			container
		);

		const parent = screen.getByTestId('header');
		const child = screen.getByTestId('logo');

		expect(parent).toContainElement(child);
	});
});*/

/*it('renders with or without a name', () => {
	act(() => {
		render(
			<Provider store={mockedStore}>
				<Header />
			</Provider>,
			container
		);
	});
	expect(screen.queryByText('dfkfkjfgsj')).not.toBeInTheDocument();
	//expect(container.textContent).toBe('Hey, stranger');
	/!*act(() => {
		render(<Hello name="Jenny" />, container);
	});
	expect(container.textContent).toBe("Hello, Jenny!");

	act(() => {
		render(<Hello name="Margaret" />, container);
	});
	expect(container.textContent).toBe("Hello, Margaret!");*!/
});*/
