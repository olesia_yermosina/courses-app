import React from 'react';
import logo from './logo.jpg';
import './Logo.css';

const Logo = () => <img className='Logo' id='logo-img' src={logo} alt='Logo' />;

export default Logo;
