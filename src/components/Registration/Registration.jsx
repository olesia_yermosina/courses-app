import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import './Registration.css';

import Input from '../../common/Input/Input';
import Button from '../../common/Button/Button';
import { fetchRegistration } from '../../store/user/thunk';

const Registration = () => {
	const dispatch = useDispatch();

	const navigate = useNavigate();

	const [name, setName] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [emailError, setEmailError] = useState('');
	const [passwordError, setPasswordError] = useState('');

	const handleNameInput = (str) => {
		setName((prevState) => str);
	};

	const handleEmailInput = (str) => {
		setEmail((prevState) => str);
		if (email) {
			if (!email.match(/.+@[^@]+\.[^@]{2,}$/)) {
				setEmailError((prevState) => "'Email' field must be email");
			} else {
				setEmailError((prevState) => '');
			}
		}
	};

	const handlePasswordInput = (str) => {
		if (password) {
			if (password.length < 6) {
				setPasswordError(
					(prevState) =>
						"'Password' field length should be 6 characters minimum"
				);
			} else {
				setPasswordError((prevState) => '');
			}
		}
		setPassword((prevState) => str);
	};

	const submitRegistration = async (e) => {
		e.preventDefault();
		if (name && !emailError && !passwordError) {
			console.log({
				name: name,
				email: email,
				password: password,
			});
			await dispatch(
				fetchRegistration({
					name: name,
					email: email,
					password: password,
				})
			);
			navigate('../login');
		} else {
			alert('Invalid fields');
		}
	};

	return (
		<>
			<div className='Registration'>
				<form onSubmit={submitRegistration}>
					<h1>Registration</h1>
					<Input
						id='name'
						labelText='Name'
						type='text'
						placeholder='Enter name'
						onChange={(e) => handleNameInput(e.target.value)}
					/>
					<Input
						id='email'
						labelText='Email'
						type='email'
						placeholder='Enter email'
						onChange={(e) => handleEmailInput(e.target.value)}
					/>
					{emailError && <p className='error'>{emailError}</p>}
					<Input
						id='password'
						labelText='Password'
						type='password'
						placeholder='Enter password'
						onChange={(e) => handlePasswordInput(e.target.value)}
					/>
					{passwordError && <p className='error'>{passwordError}</p>}
					<Button
						controls={{ buttonText: 'Registration', onClick: () => {} }}
					/>
					<p>
						If you have an account you can <Link to={'../login'}>Login</Link>
					</p>
				</form>
			</div>
		</>
	);
};

export default Registration;
