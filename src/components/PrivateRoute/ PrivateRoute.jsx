import React from 'react';
import { Navigate, Outlet } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { getUserRole } from '../../store/selectors';

const PrivateRoute = () => {
	const role = useSelector(getUserRole);

	return role === 'admin' ? <Outlet /> : <Navigate to={'/courses'} />;
};

export default PrivateRoute;
