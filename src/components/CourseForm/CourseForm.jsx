import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import './CourseForm.css';

import Input from '../../common/Input/Input';
import Button from '../../common/Button/Button';
import Textarea from '../../common/Textarea/Textarea';
import { createAuthor, fetchAuthors } from '../../store/authors/thunk';
import {
	createCourse,
	fetchCourses,
	putCourse,
} from '../../store/courses/thunk';
import { getAuthors, getCourses } from '../../store/selectors';

const CourseForm = ({ spec }) => {
	const params = useParams();

	const dispatch = useDispatch();

	const navigate = useNavigate();

	const authorsList = useSelector(getAuthors);
	const courseList = useSelector(getCourses);

	const [course, setCourse] = useState({});
	const [courseAuthors, setCourseAuthors] = useState([]);
	const [authors, setAuthors] = useState([]);
	const [newAuthor, setNewAuthor] = useState({});
	const [title, setTitle] = useState('');
	const [description, setDescription] = useState('');
	const [duration, setDuration] = useState(0);
	const [hours, setHours] = useState(0);
	const [minutes, setMinutes] = useState(0);
	const [buttonText, setButtonText] = useState('');

	const handleDurationInput = (min) => {
		if (!min.length) {
			setMinutes(0);
			setHours(0);
			setDuration(0);
		} else if (min >= 60) {
			setMinutes(+(min % 60));
			setHours(+((min - (min % 60)) / 60));
			setDuration(+min);
		} else {
			setMinutes(+min);
			setHours(0);
			setDuration(+min);
		}
	};

	const changeCourseAuthors = async (author, operation) => {
		if (operation === 'add') {
			setCourseAuthors([...courseAuthors, author]);
			let newAuthors = [...authors];
			let index = newAuthors.findIndex((el) => author.id === el.id);
			newAuthors.splice(index, 1);
			setAuthors(newAuthors);
		} else if (operation === 'delete') {
			let newCourseAuthors = [...courseAuthors];
			let index = newCourseAuthors.findIndex((el) => author.id === el.id);
			newCourseAuthors.splice(index, 1);
			setAuthors([...authors, author]);
			setCourseAuthors(newCourseAuthors);
		} else if (operation === 'new') {
			await dispatch(createAuthor(author));
			console.log(authorsList);
			let index = authors.findIndex((el) => author.name === el.name);
			console.log(index);
			setCourseAuthors([...courseAuthors, authors[index]]);
		}
	};

	const handleAuthorInput = (str) => {
		setNewAuthor((prevState) => ({ name: str }));
	};

	const handleTitleInput = (str) => {
		setTitle((prevState) => str);
	};

	const handleDescriptionInput = (str) => {
		setDescription((prevState) => str);
	};

	useEffect(() => {
		if (spec === 'update') {
			setButtonText('Update course');
			setCourse((prevState) =>
				courseList.find((course) => course.id === params.id)
			);
			if (JSON.stringify(course) !== '{}' && !title.length) {
				setTitle((prevState) => course.title);
				setDescription((prevState) => course.description);
				setDuration((prevState) => course.duration);
				setHours(
					(prevState) => (course.duration - (course.duration % 60)) / 60
				);
				setMinutes((prevState) => course.duration % 60);
				let newAuthors = [...authors];
				for (const id of course.authors) {
					let author = authors.find((el) => id === el.id);
					let index = newAuthors.findIndex((el) => id === el.id);
					setCourseAuthors((prevState) => [...prevState, author]);
					newAuthors.splice(index, 1);
					setAuthors((prevState) => [...newAuthors]);
				}
			}
		} else {
			setButtonText('Create course');
		}
		if (!courseList.length) {
			dispatch(fetchCourses());
		}
		if (!authorsList.length) {
			dispatch(fetchAuthors());
		} else if (!authors.length) {
			setAuthors([...authorsList]);
		}
	}, [
		course,
		spec,
		courseList,
		params.id,
		dispatch,
		authorsList,
		authors,
		title,
	]);

	return (
		<form className='CreateCourse'>
			<div className='top'>
				<Input
					id='title'
					labelText='Title'
					type='text'
					value={title}
					placeholder='Enter title...'
					onChange={(e) => handleTitleInput(e.target.value)}
				/>
				<Button
					controls={{
						buttonText: buttonText,
						onClick: (e) => {
							e.preventDefault();
							if (
								!title ||
								!description ||
								!duration ||
								courseAuthors.length < 1
							) {
								alert('Please, fill all fields!');
							} else {
								if (spec === 'new') {
									dispatch(
										createCourse({
											title: title,
											description: description,
											duration: duration,
											authors: courseAuthors.map((a) => a.id),
										})
									);
								} else if (spec === 'update') {
									dispatch(
										putCourse({
											id: course.id,
											course: {
												title: title,
												description: description,
												duration: duration,
												authors: courseAuthors.map((a) => a.id),
											},
										})
									);
								}
								navigate('../');
							}
						},
					}}
				/>
			</div>
			<Textarea
				id='description-txt'
				labelText='Description'
				placeholder='Enter description'
				value={description}
				minLength={2}
				rows={5}
				onInput={(e) => handleDescriptionInput(e.target.value)}
			></Textarea>
			<div className='authors'>
				<div className='left'>
					<div className='add'>
						<h3>Add author</h3>
						<Input
							id='author'
							labelText='Author name'
							type='text'
							placeholder='Enter author name...'
							onChange={(e) => handleAuthorInput(e.target.value)}
						/>
						<Button
							controls={{
								buttonText: 'Create author',
								onClick: (e) => {
									e.preventDefault();
									changeCourseAuthors(newAuthor, 'new');
								},
							}}
						/>
					</div>
					<div className='add'>
						<h3>Duration</h3>
						<Input
							id='duration'
							labelText='Duration'
							type='number'
							value={hours * 60 + minutes}
							min={0}
							placeholder='Enter duration in minutes...'
							onChange={(e) => handleDurationInput(e.target.value)}
						/>
						<p>
							Duration:{' '}
							<span>
								{hours < 10 ? '0' + hours : hours}:
								{minutes < 10 ? '0' + minutes : minutes}
							</span>{' '}
							hours
						</p>
					</div>
				</div>
				<div className='right'>
					<div className='list'>
						<h3>Authors</h3>
						<div className='authors-list'>
							{authors.length ? (
								authors.map((author) => (
									<div className='author-with-button' key={author.id}>
										<span>{author.name}</span>
										<Button
											controls={{
												buttonText: 'Add author',
												onClick: (e) => {
													e.preventDefault();
													changeCourseAuthors(author, 'add');
												},
											}}
										/>
									</div>
								))
							) : (
								<p>Author list is empty</p>
							)}
						</div>
					</div>
					<div className='list'>
						<h3>Course authors</h3>
						<div className='authors-list'>
							{courseAuthors.length ? (
								courseAuthors.map((author) => (
									<div className='author-with-button' key={author.id}>
										<span>{author.name}</span>
										<Button
											controls={{
												buttonText: 'Delete author',
												onClick: (e) => {
													e.preventDefault();
													changeCourseAuthors(author, 'delete');
												},
											}}
										/>
									</div>
								))
							) : (
								<p>Author list is empty</p>
							)}
						</div>
					</div>
				</div>
			</div>
		</form>
	);
};

CourseForm.propTypes = {
	spec: PropTypes.string,
};

export default CourseForm;
