import React from 'react';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

import CourseCard from '../CourseCard';

const mockedState = {
	user: {
		isAuth: true,
		name: 'Test Name',
		role: 'admin',
	},
	courses: [
		{
			courseInfo: {
				title: 'title',
				description: 'description',
				creationDate: '1/1/2021',
				duration: 31,
				authors: [
					'9b87e8b8-6ba5-40fc-a439-c4e30a373d36',
					'1c972c52-3198-4098-b6f7-799b45903199',
				],
				id: '66cc289e-6de9-49b2-9ca7-8b4f409d6467',
			},
		},
	],
	authors: {
		authors: [
			{
				name: 'author',
				id: '9b87e8b8-6ba5-40fc-a439-c4e30a373d36',
			},
			{
				name: 'author2',
				id: '1c972c52-3198-4098-b6f7-799b45903199',
			},
		],
	},
};
const mockedStore = {
	getState: () => mockedState,
	subscribe: jest.fn(),
	dispatch: jest.fn(),
};

const mockedCourse = {
	title: 'title',
	description: 'description',
	creationDate: '1/1/2021',
	duration: 100,
	authors: [
		'9b87e8b8-6ba5-40fc-a439-c4e30a373d36',
		'1c972c52-3198-4098-b6f7-799b45903199',
	],
	id: '66cc289e-6de9-49b2-9ca7-8b4f409d6467',
};

describe('CourseCard', () => {
	it('should create CourseCard with title', () => {
		render(
			<Provider store={mockedStore}>
				<BrowserRouter>
					<CourseCard courseInfo={mockedCourse} key={mockedCourse.id} />
				</BrowserRouter>
			</Provider>
		);

		expect(screen.getByText('title')).toBeInTheDocument();
	});

	it('should create CourseCard with description', () => {
		render(
			<Provider store={mockedStore}>
				<BrowserRouter>
					<CourseCard courseInfo={mockedCourse} key={mockedCourse.id} />
				</BrowserRouter>
			</Provider>
		);

		expect(screen.getByText('description')).toBeInTheDocument();
	});

	it('should create CourseCard with duration in the correct format', () => {
		render(
			<Provider store={mockedStore}>
				<BrowserRouter>
					<CourseCard courseInfo={mockedCourse} key={mockedCourse.id} />
				</BrowserRouter>
			</Provider>
		);

		expect(screen.getByText(/01:40/, 'i')).toBeInTheDocument();
	});

	it('should create CourseCard with created date in the correct format', () => {
		render(
			<Provider store={mockedStore}>
				<BrowserRouter>
					<CourseCard courseInfo={mockedCourse} key={mockedCourse.id} />
				</BrowserRouter>
			</Provider>
		);

		expect(screen.getByText(/01.01.2021/, 'i')).toBeInTheDocument();
	});

	it('should create CourseCard with authors list', () => {
		render(
			<Provider store={mockedStore}>
				<BrowserRouter>
					<CourseCard courseInfo={mockedCourse} key={mockedCourse.id} />
				</BrowserRouter>
			</Provider>
		);

		expect(screen.getByText(/author/, 'i')).toBeInTheDocument();
		expect(screen.getByText(/author2/, 'i')).toBeInTheDocument();
	});
});
