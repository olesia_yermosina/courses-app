import React from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import './CourseCard.css';

import Button from '../../../../common/Button/Button';
import { deleteById } from '../../../../store/courses/thunk';
import { getAuthors, getUserRole } from '../../../../store/selectors';
import { pipeDuration } from '../../../../helpers/pipeDuration';
import { dateGenerator } from '../../../../helpers/dateGeneratop';

const ADMIN = 'admin';

const CourseCard = (props) => {
	const dispatch = useDispatch();

	const authorsList = useSelector(getAuthors);
	const userRole = useSelector(getUserRole);

	let authors = '';

	const click = () => {};
	const findAuthors = () => {
		props.courseInfo.authors.forEach((id, index) => {
			const author = authorsList.find((el) => el.id === id);
			if (!author) {
				authors += '';
			} else {
				if (index === props.courseInfo.authors.length - 1) {
					authors += author.name;
				} else {
					authors += author.name + ', ';
				}
			}
		});
		if (authors.length > 25) {
			authors = authors.substring(0, 23).concat('...');
		}
	};

	if (authorsList.length !== 0 && props.courseInfo) {
		findAuthors();
	}

	return (
		<div className='CourseCard'>
			<div className='description'>
				<h2>{props.courseInfo.title}</h2>
				<p>{props.courseInfo.description}</p>
			</div>
			<div className='details'>
				<p>
					<b>Authors:</b> {authors}
				</p>
				<p>
					<b>Duration:</b> {pipeDuration(props.courseInfo.duration)} hours
				</p>
				<p>
					<b>Created:</b> {dateGenerator(props.courseInfo.creationDate)}
				</p>
				<div className='card-buttons'>
					<Link to={`${props.courseInfo.id}`}>
						<Button
							controls={{ buttonText: 'Show course', onClick: click() }}
						/>
					</Link>
					{userRole === ADMIN && (
						<Button
							controls={{
								buttonText: 'D',
								onClick: (e) => {
									e.preventDefault();
									console.log(props.courseInfo.id);
									dispatch(deleteById(props.courseInfo.id));
								},
							}}
						/>
					)}
					{userRole === ADMIN && (
						<Link to={`update/${props.courseInfo.id}`}>
							<Button
								controls={{
									buttonText: 'U',
									onClick: click(),
								}}
							/>
						</Link>
					)}
				</div>
			</div>
		</div>
	);
};

CourseCard.propTypes = {
	courseInfo: PropTypes.object,
};

export default CourseCard;
