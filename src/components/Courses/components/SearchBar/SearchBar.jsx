import React from 'react';
import PropTypes from 'prop-types';

import './SearchBar.css';

import Input from '../../../../common/Input/Input';

const SearchBar = ({ handleSearch }) => {
	return (
		<div className='SearchBar'>
			<Input
				id='search'
				labelText=''
				type='text'
				placeholder='Enter course name...'
				onChange={(e) => handleSearch(e.target.value)}
			/>
		</div>
	);
};

SearchBar.propTypes = {
	handleSearch: PropTypes.func,
};

export default SearchBar;
