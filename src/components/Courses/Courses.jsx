import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import './Courses.css';

import CourseCard from './components/CourseCard/CourseCard';
import SearchBar from './components/SearchBar/SearchBar';
import Button from '../../common/Button/Button';
import { fetchCourses } from '../../store/courses/thunk';
import { fetchAuthors } from '../../store/authors/thunk';
import { getAuthors, getCourses } from '../../store/selectors';

const Courses = () => {
	const dispatch = useDispatch();

	const courseList = useSelector(getCourses);
	const authorsList = useSelector(getAuthors);

	const [filter, setFilter] = useState('');
	const handleFilter = (str) => {
		setFilter(str);
	};

	useEffect(() => {
		if (!courseList.length) {
			dispatch(fetchCourses());
		}
		if (!authorsList.length) {
			dispatch(fetchAuthors());
		}
	});

	return (
		<div className='Courses'>
			<div className='menu'>
				<SearchBar handleSearch={handleFilter} />
				<Link to={'add'}>
					<Button
						controls={{
							buttonText: 'Add new course',
							onClick: () => {},
							testid: 'toggle',
						}}
					/>
				</Link>
			</div>
			<div className='amount'>Courses: {courseList.length}</div>
			<div>
				{courseList.length > 1 &&
					courseList
						.filter((course) => {
							return filter.toLowerCase()
								? course.title.toLowerCase().includes(filter.toLowerCase()) ||
										course.id.toLowerCase().includes(filter.toLowerCase())
								: course;
						})
						.map((course) => (
							<CourseCard courseInfo={course} key={course.id} />
						))}
				{!courseList.length && <p>Sorry, course list is empty</p>}
			</div>
		</div>
	);
};

export default Courses;
