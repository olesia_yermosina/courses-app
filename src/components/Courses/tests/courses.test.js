import React from 'react';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { Provider } from 'react-redux';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { act } from 'react-dom/test-utils';

import Courses from '../Courses';
import CourseForm from '../../CourseForm/CourseForm';

const mockedState = {
	user: {
		isAuth: true,
		name: 'Test Name',
		role: 'admin',
	},
	courses: {
		courses: [
			{
				title: 'Java',
				description:
					'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque consequatur consequuntur doloremque eum laudantium molestiae voluptate. Aliquid, culpa dignissimos ducimus eligendi esse id minus molestias natus odit quas, sapiente sed?',
				duration: 1201,
				authors: ['9b87e8b8-6ba5-40fc-a439-c4e30a373d36'],
				creationDate: '10/01/2023',
				id: '18fdb7a1-722a-4564-a9ec-c9b1c2fe906b',
			},
			{
				title: 'Angular',
				description:
					'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque consequatur consequuntur doloremque eum laudantium molestiae voluptate. Aliquid, culpa dignissimos ducimus eligendi esse id minus molestias natus odit quas, sapiente sed',
				duration: 113,
				authors: ['1c972c52-3198-4098-b6f7-799b45903199'],
				creationDate: '14/01/2023',
				id: '344163a2-fae8-479e-9f20-e387d25770b8',
			},
		],
	},
	authors: {
		authors: [
			{
				name: 'author',
				id: '9b87e8b8-6ba5-40fc-a439-c4e30a373d36',
			},
			{
				name: 'author2',
				id: '1c972c52-3198-4098-b6f7-799b45903199',
			},
		],
	},
};
const mockedStore = {
	getState: () => mockedState,
	subscribe: jest.fn(),
	dispatch: jest.fn(),
};

describe('Courses', () => {
	it('should create Courses and display amount of CourseCard equal length of courses array', () => {
		render(
			<Provider store={mockedStore}>
				<BrowserRouter>
					<Courses />
				</BrowserRouter>
			</Provider>
		);

		expect(screen.getByText(/Courses: 2/, 'i')).toBeInTheDocument();
	});

	it('should create Courses and display Empty container if courses array length is 0', () => {
		mockedState.courses.courses = [];

		render(
			<Provider store={mockedStore}>
				<BrowserRouter>
					<Courses />
				</BrowserRouter>
			</Provider>
		);

		expect(screen.getByText(/course list is empty/, 'i')).toBeInTheDocument();
	});

	it('should show CourseForm after a click on a button "Add new course"', async () => {
		const AppRoutes = () => {
			return (
				<Provider store={mockedStore}>
					<Routes>
						<Route index element={<Courses />} />
						<Route path={'/add'} element={<CourseForm spec={'new'} />} />
					</Routes>
				</Provider>
			);
		};

		render(<AppRoutes />, { wrapper: BrowserRouter });

		const button = document.querySelector('[data-testid=toggle]');
		expect(button).toBeInTheDocument();
		expect(button.innerHTML).toBe('Add new course');

		fireEvent.click(button);

		expect(screen.getByText(/Create course/, 'i')).toBeInTheDocument();
	});
});
