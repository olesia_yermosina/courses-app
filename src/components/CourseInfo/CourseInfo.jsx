import React, { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import './CourseInfo.css';

import { fetchCourses } from '../../store/courses/thunk';
import { fetchAuthors } from '../../store/authors/thunk';
import { getAuthors, getCourses } from '../../store/selectors';
import { pipeDuration } from '../../helpers/pipeDuration';
import { dateGenerator } from '../../helpers/dateGeneratop';

const CourseInfo = () => {
	const params = useParams();

	const dispatch = useDispatch();

	const authorsList = useSelector(getAuthors);
	const courseList = useSelector(getCourses);

	const [course, setCourse] = useState({});

	let authors = '';

	const findAuthors = () => {
		course.authors.forEach((id, index) => {
			const author = authorsList.find((el) => el.id === id);
			if (!author) {
				authors += '';
			} else {
				if (index === course.authors.length - 1) {
					authors += author.name;
				} else {
					authors += author.name + ', ';
				}
			}
		});
	};

	if (courseList.length !== 0 && course.authors) {
		findAuthors();
	}

	useEffect(() => {
		if (!courseList.length) {
			dispatch(fetchCourses());
		} else {
			setCourse((prevState) =>
				courseList.find((course) => course.id === params.id)
			);
		}
		if (!authorsList.length) {
			dispatch(fetchAuthors());
		}
	}, [courseList, authorsList, dispatch, params.id]);

	return (
		<div className='CourseInfo'>
			<Link to={'..'}>{'<'} Back to courses</Link>
			<h1>{course.title}</h1>
			<div className='info'>
				<div className='title'>
					<p>{course.description}</p>
				</div>
				<div className='more'>
					<p>
						<b>ID: </b>
						{course.id}
					</p>
					<p>
						<b>Duration: </b>
						{pipeDuration(course.duration)} hours
					</p>
					<p>
						<b>Created: </b>
						{course.creationDate ? dateGenerator(course.creationDate) : ''}
					</p>
					<p>
						<b>Authors:</b>
					</p>
					{authors.split(', ').map((author) => (
						<p key={author}>{author}</p>
					))}
				</div>
			</div>
		</div>
	);
};

export default CourseInfo;
