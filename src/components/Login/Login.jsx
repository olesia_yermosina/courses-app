import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import './Login.css';

import Input from '../../common/Input/Input';
import Button from '../../common/Button/Button';
import { fetchLogin } from '../../store/user/thunk';

const Login = () => {
	const dispatch = useDispatch();

	const navigate = useNavigate();

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [emailError, setEmailError] = useState('');
	const [passwordError, setPasswordError] = useState('');

	const handleEmailInput = (str) => {
		setEmail((prevState) => str);
		if (email) {
			if (!email.match(/.+@[^@]+\.[^@]{2,}$/)) {
				setEmailError((prevState) => "'Email' field must be email");
			} else {
				setEmailError((prevState) => '');
			}
		}
	};

	const handlePasswordInput = (str) => {
		setPassword((prevState) => str);
		if (password) {
			if (password.length < 6) {
				setPasswordError(
					(prevState) =>
						"'Password' field length should be 6 characters minimum"
				);
			} else {
				setPasswordError((prevState) => '');
			}
		}
	};

	const submitLogin = async (e) => {
		e.preventDefault();
		if (!emailError && !passwordError) {
			console.log({
				email: email,
				password: password,
			});
			await dispatch(
				fetchLogin({
					email: email,
					password: password,
				})
			);
			navigate('../courses');
		} else {
			alert('Invalid fields');
		}
	};

	return (
		<>
			<div className='Login'>
				<form onSubmit={submitLogin}>
					<h1>Login</h1>
					<Input
						id='email'
						value={email}
						labelText='Email'
						type='email'
						placeholder='Enter email'
						onChange={(e) => handleEmailInput(e.target.value)}
					/>
					{emailError && <p className='error'>{emailError}</p>}
					<Input
						id='password'
						value={password}
						labelText='Password'
						type='password'
						placeholder='Enter password'
						onChange={(e) => handlePasswordInput(e.target.value)}
					/>
					{passwordError && <p className='error'>{passwordError}</p>}
					<Button
						controls={{
							buttonText: 'Login',
							onClick: () => {},
						}}
					/>
					<p>
						If you have an account you can{' '}
						<Link to={'../registration'}>Registration</Link>
					</p>
				</form>
			</div>
		</>
	);
};

export default Login;
