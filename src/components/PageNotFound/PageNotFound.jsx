import React from 'react';

import './PageNotFound.css';

const PageNotFound = () => {
	return (
		<div className='PageNotFound'>
			<h1>404 Error Page</h1>
			<br />
			<h2>Page not found</h2>
		</div>
	);
};

export default PageNotFound;
