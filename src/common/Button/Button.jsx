import React from 'react';
import PropTypes from 'prop-types';

import './Button.css';

const Button = (props) => {
	return (
		<button
			data-testid={props.controls.testid}
			className={
				props.controls.buttonText.length > 1 ? 'Button' : 'SmallButton'
			}
			onClick={props.controls.onClick}
		>
			{props.controls.buttonText}
		</button>
	);
};

Button.propTypes = {
	controls: PropTypes.object,
};

export default Button;
