import React from 'react';
import PropTypes from 'prop-types';

import './Textarea.css';

const Textarea = ({ labelText, ...props }) => {
	return (
		<>
			<fieldset>
				<label htmlFor={props.id}>{labelText}</label>
				<textarea className='Textarea' {...props}></textarea>
			</fieldset>
		</>
	);
};

Textarea.propTypes = {
	labelText: PropTypes.string,
	id: PropTypes.string,
	placeholder: PropTypes.string,
	value: PropTypes.string,
	minLength: PropTypes.number,
	rows: PropTypes.number,
	onInput: PropTypes.func,
};

export default Textarea;
