import React from 'react';
import PropTypes from 'prop-types';

import './Input.css';

const Input = ({ labelText, ...props }) => {
	return (
		<>
			<fieldset>
				<label htmlFor={props.id}>{labelText}</label>
				<input className='Input' {...props} />
			</fieldset>
		</>
	);
};

Input.propTypes = {
	labelText: PropTypes.string,
	id: PropTypes.string,
	type: PropTypes.string,
	placeholder: PropTypes.string,
	onChange: PropTypes.func,
};

export default Input;
