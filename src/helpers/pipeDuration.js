export const pipeDuration = (min) => {
	let minutes = min % 60;
	let hours = (min - (min % 60)) / 60;
	if (minutes < 10) {
		minutes = `0${minutes}`;
	}
	if (hours < 10) {
		hours = `0${hours}`;
	}
	return `${hours}:${minutes}`;
};
