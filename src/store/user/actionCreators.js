import { LOGIN, LOGOUT, ME, REGISTER } from './actionTypes';

export const loginAction = (payload) => ({ type: LOGIN, payload });
export const logoutAction = () => ({ type: LOGOUT });
export const registerAction = () => ({ type: REGISTER });
export const getUserInfoAction = (payload) => ({ type: ME, payload });
