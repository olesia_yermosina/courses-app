import { LOGIN, LOGOUT, ME } from './actionTypes';

const defaultState = {
	isAuth: false,
	name: '',
	email: '',
	token: '',
	role: '',
};

export const userReducer = (state = defaultState, action) => {
	switch (action.type) {
		case LOGIN:
			return {
				...state,
				isAuth: true,
				name: action.payload.name,
				email: action.payload.email,
				token: action.payload.token,
			};
		case LOGOUT:
			return {
				...state,
				isAuth: false,
				name: '',
				email: '',
				token: '',
			};
		case ME:
			return {
				...state,
				role: action.payload.role,
			};
		default:
			return state;
	}
};
