import {
	getUserInfoAction,
	loginAction,
	logoutAction,
	registerAction,
} from './actionCreators';

export const fetchUser = () => {
	return (dispatch) => {
		fetch('http://localhost:4000/users/me', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				Authorization: localStorage.getItem('token'),
			},
		})
			.then((response) => response.json())
			.then((json) => dispatch(getUserInfoAction(json.result)));
	};
};
export const fetchLogin = (payload) => {
	return async (dispatch) => {
		await fetch('http://localhost:4000/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				email: payload.email,
				password: payload.password,
			}),
		})
			.then((response) => response.json())
			.then((json) => {
				console.log('setting storage');
				localStorage.setItem('token', json.result);
				localStorage.setItem('name', json.user.name);
				localStorage.setItem('email', json.user.email);
				dispatch(
					loginAction({
						name: json.user.name,
						email: json.user.email,
						token: json.result,
					})
				);
			});
	};
};
export const fetchLogout = () => {
	return (dispatch) => {
		fetch('http://localhost:4000/logout', {
			method: 'DELETE',
			headers: {
				'Content-Type': 'application/json',
				Authorization: localStorage.getItem('token'),
			},
		}).then((response) => dispatch(logoutAction()));
	};
};

export const fetchRegistration = (payload) => {
	return async (dispatch) => {
		await fetch('http://localhost:4000/register', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				name: payload.name,
				email: payload.email,
				password: payload.password,
			}),
		})
			.then((response) => response.json())
			.then((json) => dispatch(registerAction()));
	};
};
