import { addAuthor, getAllAuthors } from './actionCreators';

export const fetchAuthors = () => {
	return (dispatch) => {
		fetch('http://localhost:4000/authors/all')
			.then((response) => response.json())
			.then((json) => dispatch(getAllAuthors(json.result)));
	};
};

export const createAuthor = (payload) => {
	console.log(payload);
	return async (dispatch) => {
		await fetch('http://localhost:4000/authors/add', {
			method: 'POST',
			body: JSON.stringify(payload),
			headers: {
				'Content-Type': 'application/json',
				Authorization: localStorage.getItem('token'),
			},
		})
			.then((response) => response.json())
			.then((json) => dispatch(addAuthor(json.result)));
	};
};
