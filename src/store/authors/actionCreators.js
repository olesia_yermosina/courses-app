import { ADD_AUTHOR, GET_ALL_AUTHORS } from './actionTypes';

export const getAllAuthors = (payload) => ({ type: GET_ALL_AUTHORS, payload });
export const addAuthor = (payload) => ({ type: ADD_AUTHOR, payload });
