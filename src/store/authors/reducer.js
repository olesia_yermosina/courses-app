import { ADD_AUTHOR, GET_ALL_AUTHORS } from './actionTypes';

const defaultState = {
	authors: [],
};

export const authorsReducer = (state = defaultState, action) => {
	switch (action.type) {
		case GET_ALL_AUTHORS:
			return {
				...state,
				authors: [...action.payload],
			};
		case ADD_AUTHOR:
			console.log([...state.authors, action.payload]);
			return {
				...state,
				authors: [...state.authors, action.payload],
			};
		default:
			return state;
	}
};
