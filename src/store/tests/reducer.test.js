import React from 'react';
import { coursesReducer } from '../courses/reducer';
import { addCourse, getAllCourses } from '../courses/actionCreators';

const defaultState = {
	courses: [],
};

describe('coursesReducer', () => {
	it('should return the initial state', () => {
		expect(coursesReducer(undefined, { type: undefined })).toEqual({
			courses: [],
		});
	});

	it('should handle ADD_COURSE and returns new state', () => {
		const course = {
			title: 'title',
			description: 'description',
			creationDate: '9/3/2021',
			duration: 31,
			authors: [
				'9b87e8b8-6ba5-40fc-a439-c4e30a373d36',
				'1c972c52-3198-4098-b6f7-799b45903199',
			],
			id: '66cc289e-6de9-49b2-9ca7-8b4f409d6467',
		};

		expect(coursesReducer(defaultState, addCourse(course))).toEqual({
			courses: [
				{
					title: 'title',
					description: 'description',
					creationDate: '9/3/2021',
					duration: 31,
					authors: [
						'9b87e8b8-6ba5-40fc-a439-c4e30a373d36',
						'1c972c52-3198-4098-b6f7-799b45903199',
					],
					id: '66cc289e-6de9-49b2-9ca7-8b4f409d6467',
				},
			],
		});
	});

	it('should handle GET_ALL_COURSES and returns new state', () => {
		const courses = [
			{
				title: 'title',
				description: 'description',
				creationDate: '9/3/2021',
				duration: 31,
				authors: [
					'9b87e8b8-6ba5-40fc-a439-c4e30a373d36',
					'1c972c52-3198-4098-b6f7-799b45903199',
				],
				id: '66cc289e-6de9-49b2-9ca7-8b4f409d6467',
			},
		];

		expect(coursesReducer(defaultState, getAllCourses(courses))).toEqual({
			courses: [
				{
					title: 'title',
					description: 'description',
					creationDate: '9/3/2021',
					duration: 31,
					authors: [
						'9b87e8b8-6ba5-40fc-a439-c4e30a373d36',
						'1c972c52-3198-4098-b6f7-799b45903199',
					],
					id: '66cc289e-6de9-49b2-9ca7-8b4f409d6467',
				},
			],
		});
	});
});
