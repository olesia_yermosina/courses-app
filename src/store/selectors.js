export const getCourses = (state) => state.courses.courses;

export const getAuthors = (state) => state.authors.authors;

export const getIsAuth = (state) => state.user.isAuth;
export const getUserName = (state) => state.user.name;
export const getUserRole = (state) => state.user.role;
