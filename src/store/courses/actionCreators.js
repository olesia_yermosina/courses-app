import {
	ADD_COURSE,
	DELETE_COURSE,
	GET_ALL_COURSES,
	UPDATE_COURSE,
} from './actionTypes';
export const getAllCourses = (payload) => ({ type: GET_ALL_COURSES, payload });
export const addCourse = (payload) => ({ type: ADD_COURSE, payload });
export const updateCourse = (payload) => ({ type: UPDATE_COURSE, payload });
export const deleteCourse = (payload) => ({ type: DELETE_COURSE, payload });
