import {
	ADD_COURSE,
	DELETE_COURSE,
	GET_ALL_COURSES,
	UPDATE_COURSE,
} from './actionTypes';

const defaultState = {
	courses: [],
};

export const coursesReducer = (state = defaultState, action) => {
	switch (action.type) {
		case GET_ALL_COURSES:
			return {
				...state,
				courses: [...action.payload],
			};
		case ADD_COURSE:
			return {
				...state,
				courses: [...state.courses, action.payload],
			};
		case UPDATE_COURSE:
			const updArr = [...state.courses];
			const updated = updArr.findIndex((el) => el.id === action.payload.id);
			console.log(updated, action.payload);
			updArr.splice(updated, 1, action.payload);
			return {
				...state,
				courses: [...updArr],
			};
		case DELETE_COURSE:
			const newArr = [...state.courses];
			const moved = newArr.findIndex((el) => el.id === action.payload);
			newArr.splice(moved, 1);
			return {
				...state,
				courses: [...newArr],
			};
		default:
			return state;
	}
};
