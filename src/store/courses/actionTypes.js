export const GET_ALL_COURSES = 'COURSES Get All';
export const ADD_COURSE = 'COURSES Add New Course';
export const UPDATE_COURSE = 'COURSES Update Course';
export const DELETE_COURSE = 'COURSES Delete Course';
