import {
	addCourse,
	deleteCourse,
	getAllCourses,
	updateCourse,
} from './actionCreators';

export const fetchCourses = () => {
	return (dispatch) => {
		fetch('http://localhost:4000/courses/all')
			.then((response) => response.json())
			.then((json) => dispatch(getAllCourses(json.result)));
	};
};
export const deleteById = (payload) => {
	return (dispatch) => {
		fetch(`http://localhost:4000/courses/${payload}`, {
			method: 'DELETE',
			headers: {
				'Content-Type': 'application/json',
				Authorization: localStorage.getItem('token'),
			},
		}).then((response) => dispatch(deleteCourse(payload)));
	};
};
export const createCourse = (payload) => {
	console.log(payload);
	return (dispatch) => {
		fetch('http://localhost:4000/courses/add', {
			method: 'POST',
			body: JSON.stringify(payload),
			headers: {
				'Content-Type': 'application/json',
				Authorization: localStorage.getItem('token'),
			},
		})
			.then((response) => response.json())
			.then((json) => dispatch(addCourse(json.result)));
	};
};

export const putCourse = (payload) => {
	console.log(payload);
	return (dispatch) => {
		fetch(`http://localhost:4000/courses/${payload.id}`, {
			method: 'PUT',
			body: JSON.stringify(payload.course),
			headers: {
				'Content-Type': 'application/json',
				Authorization: localStorage.getItem('token'),
			},
		})
			.then((response) => response.json())
			.then((json) => dispatch(updateCourse(json.result)));
	};
};
