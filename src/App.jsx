import React, { useEffect } from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import './App.css';

import ProtectedRoute from './components/ProtectedRoute/ProtectedRoute';
import PrivateRoute from './components/PrivateRoute/ PrivateRoute';
import Header from './components/Header/Header';
import Courses from './components/Courses/Courses';
import CourseForm from './components/CourseForm/CourseForm';
import Registration from './components/Registration/Registration';
import Login from './components/Login/Login';
import CourseInfo from './components/CourseInfo/CourseInfo';
import PageNotFound from './components/PageNotFound/PageNotFound';

import { loginAction } from './store/user/actionCreators';
import { fetchUser } from './store/user/thunk';
import { getIsAuth } from './store/selectors';

function App() {
	const isAuth = useSelector(getIsAuth);

	const dispatch = useDispatch();

	useEffect(() => {
		if (localStorage.getItem('token')) {
			dispatch(
				loginAction({
					name: localStorage.getItem('name'),
					email: localStorage.getItem('email'),
					token: localStorage.getItem('token'),
				})
			);
			dispatch(fetchUser());
		}
	});

	console.log(isAuth);

	return (
		<div className='App'>
			<Header />
			<Routes>
				<Route path='/registration' element={<Registration />} />
				<Route path='/login' element={<Login />} />
				<Route element={<ProtectedRoute />}>
					<Route path='' element={<Navigate to='/courses' />} />
					<Route path='/courses'>
						<Route index element={<Courses />} />
						<Route path=':id' element={<CourseInfo />} />
						<Route element={<PrivateRoute />}>
							<Route path='add' element={<CourseForm spec={'new'} />} />
							<Route
								path='update/:id'
								element={<CourseForm spec={'update'} />}
							/>
						</Route>
					</Route>
				</Route>
				<Route path='*' element={<PageNotFound />} />
			</Routes>
		</div>
	);
}

export default App;
